export default {
    '/': 'Home',
    '/database': 'Database',
    '/files': 'Files',
    '/index': 'Index',
    '/index/view': 'View',
};
