console.log('Global JS Loaded');

const _this = {
    _retrieve: ($key) =>
    {
        const stored = sessionStorage.getItem($key) ? sessionStorage.getItem($key) : localStorage.getItem($key);
        return JSON.parse(stored);
    },

    _store: ($key, $value) =>
    {
        const _value = typeof $value == 'object' ? JSON.stringify($value) : $value;
        sessionStorage.setItem($key, _value);
        localStorage.setItem($key, _value);
    },
};

const _http = {
    post: ($url, $data = null) =>
    {
        const data = $data || new FormData();
        const host = _global.getItem('config').ws_host;
        return axios.post('//' + host + $url, data, {
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(function (response) {
            return response;
        }).catch(function (error) {
            return error.response;
        });
    }
};

const _global = {
    Http: _http,

    removeItem: ($key) =>
    {
        localStorage.removeItem($key);
        sessionStorage.removeItem($key);
    },

    getItem: ($key, $value_if_null = null) =>
    {
        if (_this._retrieve($key)) {
            return _this._retrieve($key);
        } else {
            if ($value_if_null) {
                _this._store($key, $value_if_null);
                return $value_if_null;
            } else {
                return null;
            }
        }
    },

    setItem: ($key, $value) =>
    {
        _this._store($key, $value);
        return $value;
    },

    toastr: ($html, $type) =>
    {
        let icon = '';
        switch ($type) {
            case 'success':
                icon = `<i class="fa fa-check-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'info':
                icon = `<i class="fa fa-info-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'warning':
                icon = `<i class="fa fa-exclamation-circle" aria-hidden="true"></i> &nbsp;`;
                break;
            
            case 'danger':
                icon = `<i class="fa fa-times-circle" aria-hidden="true"></i> &nbsp;`;
                break;
        
            default:
                break;
        }
        M.toast({ html: icon + $html, classes: 'bg-' + $type });
    },

    gravatarURL: ($email) =>
    {
        return 'http://www.gravatar.com/avatar/' + CryptoJS.MD5($email);
    },

    validateEmail: ($email) =>
    {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String($email).toLowerCase());
    },

    setConfig: ($key, $value) =>
    {
        const _config = _this._retrieve('config') ? _this._retrieve('config') : {};
        _config[$key] = $value;
        _this._store('config', _config);
        return $value;
    },

    strip: ($input) =>
    {
        return $('<p>').html($input).text();
    },

    onResponse: ($res) =>
    {
        if ($res) {
            if ($res.status === 200 || $res.status === 201) {
                _global.toastr($res.data, 'success');
            } else {
                if ($res.data) {
                    _global.toastr($res.data, 'danger');
                } else {
                    _global.toastr('Host Error', 'danger');
                }
            }
        } else {
            _global.toastr('Host Error', 'danger');
        }
    },
};

export default _global;