# Helix Frontend using Vue

## Build Setup

``` bash
# install dependencies
npm install

# build for production with minification
npm run build

# build for development
npm run dev
```

Then all are set
